<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
[![LinkedIn][linkedin-shield]][linkedin-url]



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://digitalpowersystems.eu/de/home-de/">
    <img src="Readme/DPS-Logo.webp" alt="Logo" width="150" height="150">
  </a>

  <h3 align="center">DPS Products</h3>

  <p align="center">
    Here you will find all DPS products and their product images, labels, datasheets and much more.
    <br />
    <a href="https://gitlab.com/DigitalPowerSystems/Products"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/DigitalPowerSystems/Products/-/issues">Report Bug</a>
  </p>
</div>


<!-- ABOUT THE PROJECT -->
## About The Project
<div align="center">
  <a href="https://digitalpowersystems.eu/de/home-de/">
      <img src="Readme/Produktpalette.webp" alt="Logo" width="700" height="700">
  </a>
</div>
<br />
<br />

Here you will find all DPS products along with their product images, labels, datasheets, and much more.

#### Products
The DPS product range includes a variety of innovative and high-performance solutions covering different application areas. The following categories and products are available:

**Power Electronics**
DPS offers a wide range of high-performance inverters and DC/DC converters. These products are designed to ensure maximum efficiency and reliability in a variety of applications. Whether in industrial environments or energy generation, DPS power electronics meet the highest standards.

**Industrial Applications**
Our customizable industrial electronics solutions are designed for robust and reliable applications. DPS ensures that devices function flawlessly even under extreme conditions. This includes tailored solutions for specific industrial requirements as well as standardized products that can be used across various industries.

**Software Solutions**
In addition to hardware products, DPS also offers comprehensive software solutions for monitoring and controlling energy systems. These software solutions include energy monitoring and control software, enabling precise analysis and optimization of energy flows. These tools are essential for maximizing efficiency and reducing operating costs.

For more information on the products and their specific details, please refer to the product documentation and datasheets available on our [product page](https://digitalpowersystems.eu/de/standard-produkte/).

If you want to report bugs or submit improvement suggestions, please visit our [issues portal](https://gitlab.com/DigitalPowerSystems/Products/-/issues).

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- CONTACT -->
## Contact

- Dr. Michael Heidinger - michael.heidinger@digitalpowersystems.eu
- Anselm Scherr    - anselm.scherr@digitalpowersystems.eu
## 

Projekt Link: [Products](https://gitlab.com/DigitalPowerSystems/Products)

<p align="right">(<a href="#readme-top">back to top</a>)</p>


[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/company/digitalpowersystems/
[product-screenshot]: Readme/Produktpalette.webp